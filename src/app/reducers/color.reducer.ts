import { createAction, on, createReducer, props, State } from "@ngrx/store";
import { Color } from "../types/Color";
const { floor, ceil, random } = Math;

const getRandomNumber = (min: number, max: number) =>
  floor(random() * (floor(max) - ceil(min)) + ceil(min));

const getRandomRGBValue = () =>
  <Color>{
    r: getRandomNumber(0, 256),
    g: getRandomNumber(0, 256),
    b: getRandomNumber(0, 256),
  };

export enum ColorAction {
  SET_RANDOM_COLOR = "[Color] Set Random Color",
  SET_COLOR = "[Color] Set Color",
}

export const colorActions = {
  setRandomColor: createAction(ColorAction.SET_RANDOM_COLOR),
  setColor: createAction(ColorAction.SET_COLOR, props<Color>()),
} as const;

export const colorReducer = createReducer(
  {
    color: <Color>{ r: 256, g: 256, b: 256 },
    // test: <Color>{ r: 0, g: 0, b: 0 },
  },
  on(colorActions.setRandomColor, (state: any) => {
    return {
      ...state,
      color: <Color>getRandomRGBValue(),
    };
  }),
  on(colorActions.setColor, (state: any, newValue: any) => {
    return { ...state, color: { r: newValue.r, g: newValue.g, b: newValue.b } };
  })
);
