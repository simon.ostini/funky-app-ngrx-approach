import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";

import { DiscoComponent } from "./components/disco/disco.component";
import { SecondarypageComponent } from "./components/secondarypage/secondarypage.component";

const ROUTES: Routes = [
  { path: "", component: DiscoComponent },
  {
    path: "secondary",
    component: SecondarypageComponent,
    pathMatch: "full",
  },
  {
    path: "secondary/:id",
    component: SecondarypageComponent,
    pathMatch: "full",
  },
  { path: "**", redirectTo: "secondary" },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(ROUTES, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
