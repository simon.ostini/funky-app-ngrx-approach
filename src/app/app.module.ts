import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { DiscoComponent } from "./components/disco/disco.component";
import { StoreModule } from "@ngrx/store";
import { colorReducer } from "./reducers/color.reducer";
import { CommonModule } from "@angular/common";
import { SecondarypageComponent } from "./components/secondarypage/secondarypage.component";
import { AppRoutingModule } from "./app-routing.module";
import { RoutingparampageComponent } from "./components/routingparampage/routingparampage.component";

@NgModule({
  declarations: [
    AppComponent,
    DiscoComponent,
    SecondarypageComponent,
    RoutingparampageComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    StoreModule.forFeature("color", colorReducer),
    StoreModule.forRoot({}),
    AppRoutingModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
