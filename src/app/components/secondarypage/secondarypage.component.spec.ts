import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondarypageComponent } from './secondarypage.component';

describe('SecondarypageComponent', () => {
  let component: SecondarypageComponent;
  let fixture: ComponentFixture<SecondarypageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SecondarypageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondarypageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
