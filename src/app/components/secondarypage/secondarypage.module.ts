import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SecondarypageComponent } from "./secondarypage.component";

@NgModule({
  declarations: [SecondarypageComponent],
  imports: [CommonModule],
})
export class SecondarypageModule {}
