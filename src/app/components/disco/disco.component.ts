import { Component, Injectable, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { colorActions } from "src/app/reducers/color.reducer";
import { Color } from "src/app/types/Color";
import { Router } from "@angular/router";

@Component({
  selector: "app-disco",
  templateUrl: "./disco.component.html",
  styleUrls: ["./disco.component.scss"],
})
export class DiscoComponent implements OnInit {
  public color$: Observable<{ color: Color }>;
  public redirectId: number = 0;
  private _defaultResetColor: Color = { r: 200, g: 200, b: 200 };

  constructor(private _colorStore: Store<any>, private _router: Router) {
    this.color$ = this._colorStore.select("color");
  }

  public onDiscoBallClick(): void {
    this._colorStore.dispatch(colorActions.setRandomColor());
  }

  public resetColor(): void {
    this._colorStore.dispatch(colorActions.setColor(this._defaultResetColor));
  }

  ngOnInit(): void {
    this._colorStore.dispatch(colorActions.setColor(this._defaultResetColor));
    // this._colorStore.select("color").subscribe((color) => {
    //   console.log(color.color);
    //   this.color$ = color.color;
    // });
  }

  public redirectToSecondary(id?: number): void {
    console.log(id);
    id !== undefined
      ? this._router.navigate(["/secondary/", id])
      : this._router.navigate(["/secondary"]);
  }
}
