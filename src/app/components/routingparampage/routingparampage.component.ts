import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-routingparampage",
  templateUrl: "./routingparampage.component.html",
  styleUrls: ["./routingparampage.component.scss"],
})
export class RoutingparampageComponent implements OnInit {
  public param!: number;

  constructor(private _route: ActivatedRoute) {}

  ngOnInit(): void {
    this.param = +(this._route.snapshot.paramMap.get("id") ?? this.param);
  }
}
