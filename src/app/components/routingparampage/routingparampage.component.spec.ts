import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingparampageComponent } from './routingparampage.component';

describe('RoutingparampageComponent', () => {
  let component: RoutingparampageComponent;
  let fixture: ComponentFixture<RoutingparampageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoutingparampageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingparampageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
